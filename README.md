# Pollution, Death, Garbage and Crime Remover Mod #
## For Cities : Skylines ##
### [Steam Workshop Link](http://steamcommunity.com/sharedfiles/filedetails/?id=769744928) ###

### Effectively removes any/all kinds of pollution (ground, water, noise and/or Dead people, Garbage, Criminals) from your game. ###

![pol-before.jpg](https://bitbucket.org/repo/LArKor/images/2553279968-pol-before.jpg)

![pol-after.jpg](https://bitbucket.org/repo/LArKor/images/526664077-pol-after.jpg)
