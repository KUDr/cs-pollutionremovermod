﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using KUDr.PollutionRemoverMod.misc;
using UnityEngine;
//using System.Security.Cryptography.Xml;

namespace KUDr.PollutionRemoverMod.settings
{
    public sealed class ModSettings : SettingGroup
    {
        public static string fileName = Path.Combine(Application.persistentDataPath, $"{Mod.name}.xml");

        public readonly PollutionOptions PollutionOptions;
        public readonly FeatureOptions FeatureOptions;

        public int m_loadSaveAllowed;

        public override IEnumerable<SettingBase> children
        {
            get
            {
                yield return PollutionOptions;
                yield return FeatureOptions;
            }
        }

        public ModSettings()
            : base(null, "PollutionRemoverModSettings", "Pollution Remover Mod Settings")
        {
            PollutionOptions = new PollutionOptions(this);
            FeatureOptions = new FeatureOptions(this);
            m_loadSaveAllowed = 1;
        }

        public override void OnChildSettingChanged(SettingBase child, string name)
        {
            base.OnChildSettingChanged(child, name);
            SaveToFile();
        }

        public void LoadFromFile()
        {
            if (Interlocked.CompareExchange(ref m_loadSaveAllowed, 0, 1) != 1) return;

            try
            {

                // Try to read xml from the settings file
                Log.Info($"Loading mod settings from {fileName}");
                var xeRoot = XLoad();
                bool loadFailed = (xeRoot == null);
                if (loadFailed) return;

                try
                {
                    xml = xeRoot;
                    Log.Info($"Loaded mod settings from {fileName}");
                }
                catch (Exception ex)
                {
                    Log.Error($"Failed to deserialize settings from {fileName}:\n{ex}");
                }
            }
            finally
            {
                m_loadSaveAllowed = 1;
            }
        }

        public void SaveToFile()
        {
            if (Interlocked.CompareExchange(ref m_loadSaveAllowed, 0, 1) != 1) return;

            try
            {
                Log.Info($"Saving mod settings to {fileName}");
                XSave(xml);
                Log.Info($"Saved mod settings to {fileName}");
            }
            finally
            {
                m_loadSaveAllowed = 1;
            }
        }

        private static XElement XLoad()
        {
            XElement xeRoot = null;
            for (int i = 0; xeRoot == null && i < 100; i++)
            {
                try
                {
                    using (var stm = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete))
                    {
                        using (var rd = new StreamReader(stm, enco))
                        {
                            xeRoot = XElement.Load(rd, LoadOptions.PreserveWhitespace);
                            rd.Close();
                            stm.Close();
                            return xeRoot;
                        }
                    }
                }
                catch (IOException ex)
                {
                    Log.Warning($"XLoad error #{i}: {ex.Message}");
                    if (!ex.Message.Contains("sharing violation")) break;
                    Thread.Sleep(10);
                }
                catch (Exception ex)
                {
                    Log.Warning($"Failed to load PRM settings from {fileName}:\n{ex}");
                    break;
                }
            }
            return null;
        }

        private static void XSave(XElement xeRoot)
        {
            try
            {
                using (var stm = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite | FileShare.Delete))
                {
                    using (var wr = new StreamWriter(stm, enco))
                    {
                        xeRoot.Save(wr, SaveOptions.None);
                        wr.Flush();
                        stm.Flush();
                        stm.SetLength(stm.Position);
                        stm.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Failed to save PRM settings to {fileName}:\n{ex}");
            }
        }
    }
}