using System;
using System.ComponentModel;
using ColossalFramework.UI;
using KUDr.PollutionRemoverMod.misc;

namespace KUDr.PollutionRemoverMod.settings
{
    public class SettingBool : SettingBase<bool>
    {
        public SettingBool(SettingGroup group, string name, string caption, string tooltip, bool defaultValue)
            : base(group, name, caption, tooltip, defaultValue)
        {

        }

        public override UISettingBase CreateUI(UIComponent parent)
        {
            Log.Debug($"Creating UI for setting: {name}");
            return new UISettingBool(parent, this);
        }
    }

    public class UISettingBool : UISettingBase<UICheckBox, SettingBool>
    {
        public UISettingBool(UIComponent parent, SettingBool setting)
        {
            m_setting = setting;
            m_ui = parent.AttachUIComponent(UITemplateManager.GetAsGameObject(kCheckBoxTemplate)) as UICheckBox;
            if (m_ui == null) throw new InsufficientMemoryException($"Can't get {kCheckBoxTemplate} for setting {setting.name}");

            m_ui.text = setting.caption;
            m_ui.tooltip = setting.tooltip;
            m_ui.isChecked = setting.value;
            m_ui.eventCheckChanged += OnUiChanged;
            m_ui.objectUserData = this;

            m_setting.PropertyChanged += OnSettingChanged;
        }

        protected virtual void OnSettingChanged(object sender, PropertyChangedEventArgs e)
        {
            if (m_ui.isChecked == m_setting.value) return;
            m_ui.isChecked = m_setting.value;
        }

        protected virtual void OnUiChanged(UIComponent component, bool value)
        {
            if (m_setting.value == m_ui.isChecked) return;
            m_setting.value = m_ui.isChecked;
        }
    }
}