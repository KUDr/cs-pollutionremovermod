using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ColossalFramework.UI;
using KUDr.PollutionRemoverMod.misc;

namespace KUDr.PollutionRemoverMod.settings
{
    public abstract class SettingGroup : SettingBase
    {
        public abstract IEnumerable<SettingBase> children { get; }

        public override string internalStrValue { get; set; }

        public override XElement xml
        {
            get
            {
                var xe = new XElement(XName.Get(name, ns));
                foreach (var child in children)
                {
                    if (!string.IsNullOrEmpty(child.tooltip)) xe.Add(new XComment(child.tooltip));
                    xe.Add(child.xml);
                }
                return xe;
            }
            set
            {
                foreach (var child in children)
                {
                    XElement xe = null;
                    try
                    {
                        xe = value.Element(XName.Get(child.name, ns));
                        if (xe == null) continue;
                        child.xml = xe;
                    }
                    catch (Exception ex)
                    {
                        Log.Warning($"Failed to update setting {child.name} from xml value {xe?.Value}\n{ex}");
                    }
                }
            }
        }

        protected SettingGroup(SettingGroup group, string name, string caption)
            : base(group, name, caption, "")
        {
        }

        public override UISettingBase CreateUI(UIComponent parent)
        {
            Log.Debug($"Creating UI for setting: {name}");
            var ui = new UISettingGroup(parent, this);
            foreach (var child in children)
            {
                child.CreateUI(ui.ui);
            }
            return ui;
        }

        public virtual void OnChildSettingChanged(SettingBase child, string name)
        {
            group?.OnChildSettingChanged(child, $"{this.name}.{name}");
        }
    }

    public class UISettingGroup : UISettingBase<UIComponent, SettingGroup>
    {
        public UISettingGroup(UIComponent parent, SettingGroup setting)
        {
            m_setting = setting;

            UIPanel panel = parent.AttachUIComponent(UITemplateManager.GetAsGameObject(kGroupTemplate)) as UIPanel;
            if (panel == null) throw new InsufficientMemoryException($"Can't get {kGroupTemplate} for setting {setting.name}");

            panel.Find<UILabel>("Label").text = setting.caption;
            panel.objectUserData = this;

            m_ui = panel.Find("Content");
            if (m_ui == null) throw new InsufficientMemoryException($"Can't get Content of {kGroupTemplate} for setting group {setting.name}");
        }
    }
}