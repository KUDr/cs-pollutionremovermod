using System;
using System.Collections.Generic;
using KUDr.PollutionRemoverMod.misc;

namespace KUDr.PollutionRemoverMod.settings
{
    public sealed class FeatureOptions : SettingGroup
    {
        public readonly SettingBool Death;
        public readonly SettingBool Garbage;
        public readonly SettingBool Crime;
        public readonly SettingBool Atractiveness;
        public readonly SettingBool Entertainment;
        public readonly SettingBool LandValue;
        public readonly SettingBool Education;

        public FeatureOptions(SettingGroup group)
            : base(group, "FeatureOptions", "Feature Options")
        {
            Death = new SettingBool(this, "Death", "Death", "Remove dead people and replace them with new children. Maximize death care coverage.", false);
            Garbage = new SettingBool(this, "Garbage", "Garbage", "Remove garbage from all buildings", false);
            Crime = new SettingBool(this, "Crime", "Crime", "Remove criminals from all buildings and maximize police coverage.", false);
            Atractiveness = new SettingBool(this, "Atractiveness", "Atractiveness", "Maximize atractivenes of whole city. Should atract more tourists.", false);
            Entertainment = new SettingBool(this, "Entertainment", "Entertainment", "Maximize entertainment in whole city.", false);
            LandValue = new SettingBool(this, "LandValue", "Land Value", "Maximize land value across your city.", false);
            Education = new SettingBool(this, "Education", "Education", "Maximize education coverage. You will still need schools to get educated citizens.", false);
        }


        public override IEnumerable<SettingBase> children
        {
            get
            {
                yield return Death;
                yield return Garbage;
                yield return Crime;
                yield return Atractiveness;
                yield return Entertainment;
                yield return LandValue;
                yield return Education;
            }
        }
    }
}