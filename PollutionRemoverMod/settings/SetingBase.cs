﻿using System;
using System.ComponentModel;
using System.Text;
using System.Xml.Linq;
using ColossalFramework.UI;

namespace KUDr.PollutionRemoverMod.settings
{
    public abstract class SettingBase : INotifyPropertyChanged
    {
        public static XNamespace xns = XNamespace.None;
        public static string ns = xns.NamespaceName;
        public static Encoding enco = new UTF8Encoding(false, true);

        public event PropertyChangedEventHandler PropertyChanged;

        public string name;
        public string caption;
        public string tooltip;
        public SettingGroup group;

        public abstract string internalStrValue { get; set; }

        public virtual XElement xml
        {
            get { return new XElement(XName.Get(name, ns), internalStrValue); }
            set
            {
                if (internalStrValue == value.Value) return;
                internalStrValue = value.Value;
                OnPropertyChanged();
            }
        }

        protected SettingBase(SettingGroup group, string name, string caption, string tooltip)
        {
            this.name = name;
            this.caption = caption;
            this.tooltip = tooltip;
            this.group = group;
        }

        protected virtual void OnPropertyChanged()
        {
            group?.OnChildSettingChanged(this, name);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public abstract UISettingBase CreateUI(UIComponent parent);
    }

    public abstract class SettingBase<T> : SettingBase
        where T : struct
    {
        public T defaultValue;
        public T m_value;

        public T value
        {
            get
            {
                return m_value;
            }
            set
            {
                if (value.Equals(m_value)) return;
                m_value = value;
                OnPropertyChanged();
            }
        }

        public override string internalStrValue
        {
            get { return value.ToString(); }
            set { this.value = (T)(Convert.ChangeType(value, typeof(T)) ?? default(T)); }
        }

        protected SettingBase(SettingGroup group, string name, string caption, string tooltip, T defaultValue)
            : base(group, name, caption, tooltip)
        {
            value = this.defaultValue = defaultValue;
        }
    }

    public abstract class UISettingBase
    {
        public static readonly string kCheckBoxTemplate = "OptionsCheckBoxTemplate";
        public static readonly string kSliderTemplate = "OptionsSliderTemplate";
        public static readonly string kDropdownTemplate = "OptionsDropdownTemplate";
        public static readonly string kTextfieldTemplate = "OptionsTextfieldTemplate";
        public static readonly string kButtonTemplate = "OptionsButtonTemplate";
        public static readonly string kGroupTemplate = "OptionsGroupTemplate";

        public abstract UIComponent ui { get; }
        public abstract SettingBase setting { get; }

        protected UISettingBase()
        {
        }
    }

    public abstract class UISettingBase<TUI, TSetting> : UISettingBase
        where TUI : UIComponent
        where TSetting : SettingBase
    {
        public TUI m_ui;
        public TSetting m_setting;

        public override UIComponent ui => m_ui;
        public override SettingBase setting => m_setting;

        protected UISettingBase()
        {
        }
    }
}
