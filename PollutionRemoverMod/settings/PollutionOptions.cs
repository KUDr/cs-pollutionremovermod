using System;
using System.Collections.Generic;
using KUDr.PollutionRemoverMod.misc;

namespace KUDr.PollutionRemoverMod.settings
{
    public sealed class PollutionOptions : SettingGroup
    {
        public readonly SettingBool CleanGround;
        public readonly SettingBool CleanWater;
        public readonly SettingBool SilentCity;

        public override IEnumerable<SettingBase> children
        {
            get
            {
                yield return CleanGround;
                yield return CleanWater;
                yield return SilentCity;
            }
        }

        public PollutionOptions(SettingGroup group)
            : base(group, "PollutionOptions", "Pollution Options")
        {
            CleanGround = new SettingBool(this, "CleanGround", "Clean Ground", "Remove ground pollution", true);
            CleanWater = new SettingBool(this, "CleanWater", "Clean Water", "Remove water pollution", true);
            SilentCity = new SettingBool(this, "SilentCity", "Silent City", "Remove noise pollution", true);
        }
    }
}