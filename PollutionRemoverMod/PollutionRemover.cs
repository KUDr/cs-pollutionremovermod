﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ColossalFramework;
using ICities;
using KUDr.PollutionRemoverMod.misc;
using KUDr.PollutionRemoverMod.settings;
using UnityEngine;

namespace KUDr.PollutionRemoverMod
{
    public partial class PollutionRemover : ThreadingExtensionBase
    {
        public static PollutionRemover instance { get; protected set; }

        public static ushort numCitizenUnitChunks = 32;
        public static ushort numBuildingChunks = 32;

        public Mod m_mod;
        public ulong m_updateCount = 0;
        public uint m_frameIndex;
        public byte m_frame_16;

        public Action[] m_handers;

        public ushort m_nextCitizenUnitChunk;

        public ushort m_nextBuildingChunk;

        public int buildingsVisited;
        public int citizenUnitsProcessed;
        public int citizensProcessed;
        public int deathRemoved;
        public int garbageRemoved;
        public int crimeRemoved;

        public int buildingsVisitedTotal;
        public int citizenUnitsProcessedTotal;
        public int citizensProcessedTotal;
        public int deathRemovedTotal;
        public int garbageRemovedTotal;
        public int crimeRemovedTotal;

        public string name => $"{m_mod?.Name}.{GetType().Name}";

        public static bool InGame
        {
            get
            {
                var loading = instance?.managers.loading;
                if (loading == null) return false;
                return loading.loadingComplete && (loading.currentMode == AppMode.Game);
            }
        }

        public PollutionRemover()
        {
            instance = this;
            m_mod = Mod.instance;

            m_handers = new Action[]
            {
                CleanGroundStep,
                CleanWaterStep,
                SilentCityStep,
                DeathStep,
                GarbageStep,
                CrimeStep,
                AttractivenessStep,
                EntertainmentStep,
                LandValueStep,
                EducationStep,
                //CitizenUnitStep,
                BuildingStep,

            };

        }

        public string GetName() => name;

        public override void OnCreated(IThreading threading)
        {
            base.OnCreated(threading);
            Log.Info($"PR: created");
        }

        public override void OnReleased()
        {
            base.OnReleased();
            Log.Info($"PR released");
            Log.Close();
            instance = null;
        }

        public override void OnAfterSimulationFrame()
        {
            if (!InGame) return;

            if (SimulationManager.instance.SimulationPaused)
            {
                Log.Close();
                return;
            }

            m_frameIndex = SimulationManager.instance.m_currentFrameIndex;

            m_frame_16 = (byte) (m_frameIndex & 15);

            var handlerIndex = m_frame_16;

            if (m_handers.Length <= handlerIndex) return;

            Action handler = m_handers[handlerIndex];

            handler();

            if ((m_updateCount % 1000) == 0)
                Log.Debug($"{GetName()}: Update #{m_updateCount}, Frame #{m_frameIndex}");

            m_updateCount++;

            LogStats();
        }

        private void LogStats()
        {
            if (buildingsVisited == 0) return;

            buildingsVisitedTotal += buildingsVisited;
            citizenUnitsProcessedTotal += citizenUnitsProcessed;
            citizensProcessedTotal += citizensProcessed;
            deathRemovedTotal += deathRemoved;
            garbageRemovedTotal += garbageRemoved;
            crimeRemovedTotal += crimeRemoved;

            var sb = new StringBuilder();
            sb.Append($"PRM: Sim #{SimulationManager.instance.m_currentFrameIndex} B:{buildingsVisited}/{buildingsVisitedTotal}");

            if (Mod.settings.FeatureOptions.Death.value)
                sb.Append($" U:{citizenUnitsProcessed}/{citizenUnitsProcessedTotal} C:{citizensProcessed}/{citizensProcessedTotal} D:{deathRemoved}/{deathRemovedTotal}");

            if (Mod.settings.FeatureOptions.Garbage.value)
                sb.Append($" G:{garbageRemoved}/{garbageRemovedTotal}");

            if (Mod.settings.FeatureOptions.Crime.value)
                sb.Append($" CR:{crimeRemoved}/{crimeRemovedTotal}");

            Log.Debug(sb.ToString());

            buildingsVisited = 0;
            citizenUnitsProcessed = 0;
            citizensProcessed = 0;
            deathRemoved = 0;
            garbageRemoved = 0;
            crimeRemoved = 0;
            
        }


        void CleanGroundStep()
        {
            if (!Mod.settings.PollutionOptions.CleanGround.value) return;
            
            Singleton<NaturalResourceManager>.instance.AddPollutionDisposeRate(10000);
        }

        void CleanWaterStep()
        {
            if (!Mod.settings.PollutionOptions.CleanWater.value) return;

            Singleton<TerrainManager>.instance.WaterSimulation.AddPollutionDisposeRate(10000);
        }

        void SilentCityStep()
        {
            if (!Mod.settings.PollutionOptions.SilentCity.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.NoisePollution, -100000);
        }

        void DeathStep()
        {
            if (!Mod.settings.FeatureOptions.Death.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.DeathCare, 100000);
        }

        void GarbageStep()
        {
            if (!Mod.settings.FeatureOptions.Garbage.value) return;
            
            // Is it feasible to tell that whole city has garbage service radius?
        }

        void CrimeStep()
        {
            if (!Mod.settings.FeatureOptions.Crime.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.CrimeRate, -100000);
            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.PoliceDepartment, 100000);
        }

        void AttractivenessStep()
        {
            if (!Mod.settings.FeatureOptions.Atractiveness.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.Attractiveness, 100000);
        }

        void EntertainmentStep()
        {
            if (!Mod.settings.FeatureOptions.Entertainment.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.Entertainment, 100000);
        }

        void LandValueStep()
        {
            if (!Mod.settings.FeatureOptions.LandValue.value) return;

            ImmaterialResourceManager.instance.AddResource(ImmaterialResourceManager.Resource.LandValue, 100000);
        }

        void EducationStep()
        {
            if (!Mod.settings.FeatureOptions.Education.value) return;

            Singleton<ImmaterialResourceManager>.instance.AddResource(ImmaterialResourceManager.Resource.EducationUniversity, 100000);
            Singleton<ImmaterialResourceManager>.instance.AddResource(ImmaterialResourceManager.Resource.EducationHighSchool, 100000);
            Singleton<ImmaterialResourceManager>.instance.AddResource(ImmaterialResourceManager.Resource.EducationElementary, 100000);

            DistrictManager dm = Singleton<DistrictManager>.instance;
            District city = dm.m_districts.m_buffer[0];
            city.m_productionData.m_tempEducation1Capacity += 1000000u;
            city.m_productionData.m_tempEducation2Capacity += 1000000u;
            city.m_productionData.m_tempEducation3Capacity += 1000000u;
        }

        void CitizenUnitStep()
        {
            var cm = CitizenManager.instance;
            var cub = cm.m_units.m_buffer;

            var chunkSize = cub.Length / numCitizenUnitChunks;

            var begin = (uint)(m_nextCitizenUnitChunk * chunkSize);
            var end = (uint)Math.Min(begin + chunkSize, cub.Length);

            m_nextCitizenUnitChunk += 1;
            m_nextCitizenUnitChunk %= numBuildingChunks;

            for (uint i = begin; i < end; i++)
            {
                if ((cub[i].m_flags & CitizenUnit.Flags.Created) != CitizenUnit.Flags.None) continue;

                CitizenUnitDeathStep(i, ref cub[i]);
            }

        }

        void CitizenUnitDeathStep(uint cuId, ref CitizenUnit cu)
        {
            if (!Mod.settings.FeatureOptions.Death.value)
            {
                //Log.Debug($"PRM.CuDeath Cu={cuId} ignored");
                return;
            }

            var cm = CitizenManager.instance;
            var cb = cm.m_citizens.m_buffer;
            var cib = cm.m_instances.m_buffer;


            for (int i = 0; i < 5; i++)
            {
                uint citizenId = cu.GetCitizen(i);
                if (citizenId != 0u)
                {
                    if (cb[citizenId].Dead /*&& cb[citizenId].CurrentLocation != Citizen.Location.Moving*/)
                    {
                        ushort citizenInstanceId = cb[citizenId].m_instance;
                        if (citizenInstanceId != 0)
                        {
                            cb[citizenId].m_instance = 0;
                            cib[citizenInstanceId].m_citizen = 0u;
                            cm.ReleaseCitizenInstance(citizenInstanceId);
                        }
                        var family = cb[citizenId].m_family;
                        cm.ReleaseCitizen(citizenId);
                        cm.CreateCitizen(out citizenId, 5, family, ref SimulationManager.instance.m_randomizer);
                        deathRemoved++;
                    }
                    citizensProcessed++;
                }
            }
            //Log.Debug($"PRM.CuDeath Cu={cu} Rem={deathRemoved}/{citizensProcessed}");
        }

        void BuildingStep()
        {
            var bm = BuildingManager.instance;
            var bb = bm.m_buildings.m_buffer;
            var chunkSize = bb.Length / numBuildingChunks;

            var begin = (uint)(m_nextBuildingChunk * chunkSize);
            var end = (uint)Math.Min(begin + chunkSize, bb.Length);

            m_nextBuildingChunk += 1;
            m_nextBuildingChunk %= numBuildingChunks;

            for (uint i = begin; i < end; i++)
            {
                if ((bb[i].m_flags & Building.Flags.Created) == Building.Flags.None) continue;
                
                BuildingDeathStep(i, ref bb[i]);
                BuildingGarbageStep(i, ref bb[i]);
                BuildingCrimeStep(i, ref bb[i]);

                buildingsVisited++;
            }
            //Log.Debug($"PRM.Bu Pro={processed}");
        }

        void BuildingDeathStep(uint buildingId, ref Building b)
        {
            if (!Mod.settings.FeatureOptions.Death.value)
            {
                //Log.Debug($"PRM.BuDeath Bu={buildingId} ignored");
                return;
            }

            var cm = CitizenManager.instance;
            var cub = cm.m_units.m_buffer;

            for (var cuId = b.m_citizenUnits; cuId != 0; cuId = cub[cuId].m_nextUnit)
            {
                CitizenUnitDeathStep(cuId, ref cub[cuId]);
                citizenUnitsProcessed++;
            }
            //Log.Debug($"PRM.BuDeath Bu={buildingId} Cus={processed} Rem={removed}");
        }

        void BuildingGarbageStep(uint buildingId, ref Building b)
        {
            if (!Mod.settings.FeatureOptions.Garbage.value) return;

            garbageRemoved += b.m_garbageBuffer;
            b.m_garbageBuffer = 0;
        }

        void BuildingCrimeStep(uint buildingId, ref Building b)
        {
            if (!Mod.settings.FeatureOptions.Crime.value) return;

            crimeRemoved += b.m_crimeBuffer;
            b.m_crimeBuffer = 0;
        }
    }
}
