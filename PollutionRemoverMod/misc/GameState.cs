namespace KUDr.PollutionRemoverMod.misc
{
    public class GameState
    {
        public static readonly string s_failureText = "failed because LoadingManager is missing";
        public static readonly string s_successText = "goes on";

        private static GameState s_instance;
        protected static GameState instance => s_instance ?? (s_instance = Create());

        public static bool InGame => IsLoaded && (UpdateMode == SimulationManager.UpdateMode.LoadGame || UpdateMode == SimulationManager.UpdateMode.NewGame);

        public static bool IsLoaded => instance?.m_isLoaded ?? false;
        public static SimulationManager.UpdateMode UpdateMode => SimulationManager.instance?.m_metaData?.m_updateMode ?? SimulationManager.UpdateMode.Undefined;

        public static GameState Create()
        {
            var lm = LoadingManager.instance;
            Log.Info($"GameState Create {((lm == null) ? s_failureText : s_successText)}", true);
            return (lm == null) ? null : new GameState();
        }

        private bool m_isLoaded;

        private SimulationManager.UpdateMode m_lastUpdateMode = SimulationManager.UpdateMode.Undefined;

        private GameState()
        {
            LoadingManager.instance.m_introLoaded += OnIntroLoaded;
            LoadingManager.instance.m_levelLoaded += OnLevelLoaded;
            LoadingManager.instance.m_levelUnloaded += OnLevelUnloaded;
        }

        private void OnIntroLoaded()
        {
            Log.Info($"IntroLoaded");
        }

        private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
        {
            m_isLoaded = true;
            m_lastUpdateMode = updateMode;
            Log.Info($"LevelLoaded {updateMode}");
        }

        private void OnLevelUnloaded()
        {
            m_isLoaded = false;
            Log.Info($"LevelUnloaded {m_lastUpdateMode}");
        }
    }
}