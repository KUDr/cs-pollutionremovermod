﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

namespace KUDr.PollutionRemoverMod.misc
{

    internal static class Log
    {
        public enum LogLevel
        {
            Debug,
            Info,
            Warn,
            Error,
            None,
        }

        static readonly string Prefix = $"{typeof(Mod).Assembly.GetName().Name}: ";

        private static readonly string fileName = Path.Combine(Application.dataPath, $"{Mod.name}.log");
        private static LogLevel gameMinLogLevel = LogLevel.Info;

        private static StreamWriter wr;

        static Log()
        {
            File.Delete(fileName);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Close()
        {
            if (wr != null)
            {
                wr.Flush();
                wr.Close();
                wr.Dispose();
                wr = null;
            }
        }

        [Conditional("DEBUG")]
        public static void Debug(string s, bool close = false)
        {
                LogToFile(s, LogLevel.Debug, close);
                if (LogLevel.Debug >= gameMinLogLevel)
                    UnityEngine.Debug.Log(Prefix + s);
        }

        public static void Info(string s, bool close = false)
        {
            LogToFile(s, LogLevel.Info, close);
            if (LogLevel.Info >= gameMinLogLevel)
                UnityEngine.Debug.Log(Prefix + s);
        }

        public static void Warning(string s, bool close = false)
        {
            LogToFile(s, LogLevel.Warn, close);
            if (LogLevel.Warn >= gameMinLogLevel)
                UnityEngine.Debug.LogWarning(Prefix + s + ": " + (new System.Diagnostics.StackTrace()).ToString());
        }

        public static void Error(string s, bool close = false)
        {
            LogToFile(s, LogLevel.Error, close);
            if (LogLevel.Error >= gameMinLogLevel)
                UnityEngine.Debug.LogError(Prefix + s + " " + (new System.Diagnostics.StackTrace()).ToString());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void LogToFile(string msg, LogLevel level, bool close)
        {
            try
            {
                var now = DateTime.Now;
                var nowStr = now.ToString("HH:mm:ss.fff zz");
                var levelStr = level.ToString().ToUpperInvariant();
                if (wr == null)
                {
                    wr = File.Exists(fileName) ? File.AppendText(fileName) : File.CreateText(fileName);
                }
                wr.WriteLine($"{nowStr} {levelStr,-5} {msg}");

                if (level >= LogLevel.Warn)
                {
                    wr.WriteLine((new StackTrace()).ToString());
                    wr.WriteLine();
                }
            }
            catch
            {
                ;
            }
            finally
            {
                if (close || !GameState.IsLoaded)
                {
                    var wrTemp = wr;
                    wr = null;
                    wrTemp?.Dispose();
                }

            }
        }
    }
}
