﻿using System;
using System.Linq;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.UI;
using ICities;
using KUDr.PollutionRemoverMod.misc;
using KUDr.PollutionRemoverMod.settings;
using UnityEngine;

namespace KUDr.PollutionRemoverMod {
    public class Mod : IUserMod
    {
        public static Mod instance { get; private set; }

        public static string name => "PollutionRemoverMod";

        public string Name => name;

        public string Description => "Can completely remove pollution (ground, water and noise), also dead people, garbage and crime. Can also improve entertainment, atractiveness, lang value and education of your city.";

        public static ModSettings settings;

        public UISettingGroup m_settingsUI;

        public Mod()
        {
            instance = this;
        }

        public void OnSettingsUI(UIHelperBase root)
        {
            try
            {
                settings = new ModSettings();
                settings.LoadFromFile();
                var parent = (UIComponent) ((UIHelper)root).self;
                m_settingsUI = (UISettingGroup)settings.CreateUI(parent);
            }
            catch (Exception ex)
            {
                Log.Error($"Unable to create settings UI:\n{ex}");
            }
        }
    }
}
